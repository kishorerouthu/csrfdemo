var _WM_APP_PROPERTIES = {
  "activeTheme" : "default",
  "defaultLanguage" : "en",
  "displayName" : "PrefabImport",
  "homePage" : "Main",
  "name" : "PrefabImport",
  "platformType" : "DEFAULT",
  "supportedLanguages" : "en",
  "type" : "PREFAB",
  "version" : "1.0"
};