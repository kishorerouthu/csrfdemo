var _WM_APP_PROPERTIES = {
  "activeTheme" : "material",
  "defaultLanguage" : "en",
  "displayName" : "CsrfDemo",
  "homePage" : "Main",
  "name" : "CsrfDemo",
  "platformType" : "WEB",
  "supportedLanguages" : "en",
  "type" : "APPLICATION",
  "version" : "1.0"
};